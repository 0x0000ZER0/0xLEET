int 
searchInsert(int *nums, int numsSize, int target) {
        int i;
        i = 0;
    
        while (i < numsSize) {
                if (target <= nums[i])
                        break;

                ++i;
        }
        
        return i;
}
