//NOTE: The returned array must be malloced, assume caller calls free().
int* 
decode(int *encoded, int encodedSize, int first, int *returnSize) {
        *returnSize = encodedSize + 1;
        
        int *out;
        out = malloc(*returnSize * sizeof (int));
        out[0] = first;   
        
        for (int i = 0; i < encodedSize; ++i)
                out[i + 1] = out[i] ^ encoded[i];
        
        return out;
}
