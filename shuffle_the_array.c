//NOTE: The returned array must be malloced, assume caller calls free().
int* 
shuffle(int *nums, int numsSize, int n, int *returnSize) {
        *returnSize = numsSize;
        
        int *out;
        out = malloc(sizeof (int) * numsSize);
        
        for (int i = 0; i < n; ++i) {
                out[i * 2 + 0] = nums[i + 0];
                out[i * 2 + 1] = nums[i + n];
        }
        
        return out;
}
