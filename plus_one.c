int*
plusOne(int *digits, int digitsSize, int *returnSize) {        
        int carry;
        carry = 0;

        for (int i = digitsSize - 1; i >= 0; --i) {
                if (digits[i] != 9) {
                        ++digits[i];
                        carry = 0;
                        break;
                } else {
                        digits[i] = 0;
                        carry = 1;
                }
        }
        

        if (carry == 0) {
                *returnSize = digitsSize;
                return digits;
        } else {
                *returnSize = digitsSize + 1;
        
                int *out;
                // the problem assumes that someone will free for us.
                out = malloc(sizeof (int) * (*returnSize));
        
                memcpy(out + 1, digits, sizeof (int) * digitsSize);
                out[0] = 1;
                
                return out;
        }
}
