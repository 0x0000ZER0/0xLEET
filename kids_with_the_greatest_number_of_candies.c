//NOTE: The returned array must be malloced, assume caller calls free().
bool* 
kidsWithCandies(int *candies, int candiesSize, int extraCandies, int *returnSize) {
        *returnSize = candiesSize;
        
        int max;        
        max = 0; // 2nd constraint says: 1 <= candies[i] <= 100
        
        for (int i = 0; i < candiesSize; ++i)
                if (max < candies[i])
                        max = candies[i];
        
        bool *out;
        out = malloc(candiesSize * sizeof (bool));
        
        for (int i = 0; i < candiesSize; ++i)
                if (candies[i] + extraCandies >= max)
                        out[i] = true;
                else
                        out[i] = false;
        
        return out;
}
