/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* 
runningSum(int *nums, int numsSize, int *returnSize) {
        int *out;
        out = calloc(sizeof (int), numsSize);

        for (int i = 0; i < numsSize; ++i) {
                for (int j = 0; j <= i; ++j)
                        out[i] += nums[j];
        }
        
        *returnSize = numsSize;
        
        return out;
}
