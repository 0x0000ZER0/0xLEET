int* 
twoSum(int *nums, 
       int  nums_size, 
       int  target, 
       int *return_size){
        for (int i = 0; i < nums_size; ++i)
        for (int j = i + 1; j < nums_size; ++j)
                if (nums[i] + nums[j] == target) {
                        *return_size = 2;

                        int *ptr;
                        ptr = malloc(2 * sizeof (int));
                    
                        ptr[0] = i;
                        ptr[1] = j;
                    
                        return ptr;
                }
    
        return NULL;
}
