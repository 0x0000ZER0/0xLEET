/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

typedef struct ListNode node;

node*
addTwoNumbers(node *l1, node *l2) {
        int carry;
        carry = 0;

        int digit;
        
        digit = l1->val + l2->val;
        
        if (carry > 0) {
                ++digit;
                --carry;
        }

        if (digit > 9) {
                digit = digit % 10;
                ++carry;
        } 
        node *l;
        l = malloc(sizeof (node));
        
        l->val  = digit;
        l->next = NULL;
       
        l1 = l1->next;
        l2 = l2->next;
       
        node *t;
        t = l;
       
        while (l1 != NULL && l2 != NULL) {
                digit = l1->val + l2->val;
                
                if (carry > 0) {
                        ++digit;
                        --carry;
                }
                
                if (digit > 9) {
                        digit = digit % 10;
                        ++carry;
                } 

                l1 = l1->next;
                l2 = l2->next;
                
                t->next       = malloc(sizeof (node));
                t->next->val  = digit;
                t->next->next = NULL;
                
                t = t->next;
        }
        
        while (l1 != NULL) {
                digit = l1->val;
        
                if (carry > 0) {
                        ++digit;
                        --carry;
                }
                
                if (digit > 9) {
                        digit = digit % 10;
                        ++carry;
                } 
        
                l1 = l1->next;   
                
                t->next       = malloc(sizeof (node));
                t->next->val  = digit;
                t->next->next = NULL;
                
                t = t->next;
        }
        
        while (l2 != NULL) {
                digit = l2->val;
        
                if (carry > 0) {
                        ++digit;
                        --carry;
                }
                
                if (digit > 9) {
                        digit = digit % 10;
                        ++carry;
                } 
        
                l2 = l2->next;   
                
                t->next       = malloc(sizeof (node));
                t->next->val  = digit;
                t->next->next = NULL;
                
                t = t->next;
        }
        
        
        if (carry > 0) {
                t->next       = malloc(sizeof (node));
                t->next->val  = 1;
                t->next->next = NULL;
                
                t = t->next;
        }
        
        return l;
}
