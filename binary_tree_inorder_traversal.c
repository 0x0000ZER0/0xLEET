/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
typedef struct TreeNode node;

void
count(node *item, int *cnt) {
        ++(*cnt);

        if (item->left != NULL)
                count(item->left, cnt);
                
        if (item->right != NULL)
                count(item->right, cnt);
}

void
store(node *item, int *arr, int* idx) {
        if (item->left)
                store(item->left, arr, idx);

        arr[*idx] = item->val;
        ++(*idx);

        if (item->right)
                store(item->right, arr, idx);
}

int* 
inorderTraversal(node *root, int *returnSize) {
        *returnSize = 0;
        
        if (root == NULL) {
                return malloc(sizeof (int));
        }
        
        count(root, returnSize);

        int *arr;
        arr = malloc(*returnSize * sizeof (int));
        
        int idx;
        idx = 0;
        
        store(root, arr, &idx);
        
        return arr;
}
