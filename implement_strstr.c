
int 
strStr(char *haystack, char *needle) {
        if (needle[0] == '\0')
                return 0;

        for (int i = 0; haystack[i] != '\0'; ++i) {
                if (haystack[i] == needle[0])
                        for (int j = 0, k = i; haystack[k] != '\0' && needle[j] == haystack[k]; ++j, ++k)
                                if (needle[j + 1] == '\0')
                                        return i;
        }
        
        return -1;
}
