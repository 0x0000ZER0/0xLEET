bool 
isPalindrome(long x) {
        if (x < 0)
                return false;

        long t;
        t = x;
        
        long n = 0;
        while (t != 0) {
                n = (n * 10) + (t % 10);
                t =  t / 10;
        }        

        return x == n;
}
