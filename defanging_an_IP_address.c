char* 
defangIPaddr(char *address) {
        size_t len;
        len = strlen(address);

        char *def;
        def = malloc(len + 3 * 2 + 1);

        char *tmp;
        tmp = def;
        while (*address != '\0') {
                if (*address == '.') {
                        *(tmp + 0) = '[';
                        *(tmp + 1) = '.';
                        *(tmp + 2) = ']';

                        tmp += 3;
                } else {
                        *tmp = *address;
                        ++tmp;
                }
                
                ++address;                
        }
        
        def[len + 2 * 3] = '\0';
        
        return def;
}
