int 
romanToInt(char *s) {
        static int T[7][2] = {
                { 'I', 1    },
                { 'V', 5    },
                { 'X', 10   },
                { 'L', 50   },
                { 'C', 100  },
                { 'D', 500  },
                { 'M', 1000 },
        };

        if (*(s + 1) == '\0')
                for (int i = 0; i < 7; ++i)
                        if (*s == T[i][0])
                                return T[i][1];

        int c;
        int n;
        int r;
        
        c = 0;
        n = 0;
        r = 0;
 
        while (*s != '\0' && *(s + 1) != '\0') {
                for (int i = 0; i < 7; ++i) {
                        if (*s == T[i][0])
                                c = T[i][1];
                        if (*(s+1) == T[i][0])
                                n = T[i][1];
                }
                
                if (c >= n)
                        r += c;
                else 
                        r -= c;
                ++s;
        }
        r += n;
        
        return r;
}
